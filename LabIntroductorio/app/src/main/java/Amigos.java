import android.content.pm.InstrumentationInfo;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jose on 21/02/2018.
 */


public class Amigos implements Parcelable
{
    private String nombre;
    private int edad;
    private String carrera;

    public Amigos(String n,int e, String c)
    {
        this.nombre=n;
        e=edad;
        c=carrera;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getCarrera() {
        return carrera;
    }

    protected Amigos(Parcel in) {
        nombre = in.readString();
        edad = in.readInt();
        carrera = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeInt(edad);
        dest.writeString(carrera);
    }



    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Amigos> CREATOR = new Parcelable.Creator<Amigos>() {
        @Override
        public Amigos createFromParcel(Parcel in) {
            return new Amigos(in);
        }

        @Override
        public Amigos[] newArray(int size) {
            return new Amigos[size];
        }
    };
}