package com.jose.labintroductorio;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by jose on 21/02/2018.
 */


public class Amigos implements Parcelable
{
    //declaracion de variables
    private String nombre;
    private int edad;
    private String carrera;
    private ArrayList<Gustos> gustos;


    //constructor
    public Amigos(String n, int e, String c, String gusto1, String gusto2)
    {
        this.nombre=n;
        this.edad=e;
        this.carrera=c;
        gustos=new ArrayList<Gustos>();
        Gustos g=new Gustos(gusto1);
        this.gustos.add(g);

        Gustos i=new Gustos(gusto2);
        this.gustos.add(i);
    }
    public Amigos(){}

    //todos los gets
    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getCarrera() {
        return carrera;
    }

    protected Amigos(Parcel in) {
        nombre = in.readString();
        edad = in.readInt();
        carrera = in.readString();
    }

    public ArrayList<Gustos> getGustos()
    {
        return this.gustos;
    }

    public String getGustos2(ArrayList<Gustos> gustosL)
    {
        String concatenarGustos="";
        for (Gustos i:gustosL)
        {
            concatenarGustos+=i.getGusto1()+"\n";

        }

        return concatenarGustos;

    }

    //metodos del Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeInt(edad);
        dest.writeString(carrera);
        dest.writeTypedList(gustos);

    }

    @Override
    public String toString() {
        return nombre;

    }

    @SuppressWarnings("unused")
    public static final Creator<Amigos> CREATOR = new Creator<Amigos>() {
        @Override
        public Amigos createFromParcel(Parcel in) {
            return new Amigos(in);
        }

        @Override
        public Amigos[] newArray(int size) {
            return new Amigos[size];
        }
    };
}