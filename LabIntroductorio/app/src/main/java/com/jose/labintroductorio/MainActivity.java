package com.jose.labintroductorio;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<Amigos> amigosL= new ArrayList<Amigos>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //creacion de objetos de tipo amigos
        final Amigos oscar=new Amigos("Oscar Juarez",19, "Compu","Rojo","Corinto");
        amigosL.add(oscar);
        final Amigos javier=new Amigos("Javier Carpio",18, "Compu","Rojo","Azul");
        amigosL.add(javier);
        final Amigos rodrigo=new Amigos("Rodrigro Zea",18, "Compu","Rojo","Verde");
        amigosL.add(rodrigo);
        final Amigos pablo=new Amigos("Pablo Pelaez",19, "Admin","Rojo","Rosado");
        amigosL.add(pablo);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        //se crea un objeto de tipo listView referenciado al que esta en la GUI
        listView=findViewById(R.id.lista);
        //Se crea un arrayAdapter
        final ArrayAdapter<Amigos> adapter=new ArrayAdapter<Amigos>(this,android.R.layout.simple_list_item_1,amigosL );
        //le decimos que adaptador vamos a usar en la listView, este adaptador se encarga de acomodar toda la DATA en la listView
        listView.setAdapter(adapter);


        //como saber a cual le da click y asi mandar la data hasta el otro activity
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // se crea el intet y le decimos de que activity vamos a sacar la informacion y para donde va a ir (activity2)
                Intent in= new Intent(MainActivity.this,Activity2.class);
                //se utiliza .putExtra para mandar la data, poniendo el id de primero y posteriormente el valor
                in.putExtra("nombre",adapter.getItem(position).getNombre());
                in.putExtra("edad",adapter.getItem(position).getEdad());
                in.putExtra("carrera",adapter.getItem(position).getCarrera());
                in.putExtra("gustos",adapter.getItem(position).getGustos());

                //ejecutar intent creado anteriormente
                startActivityForResult(in,1);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
